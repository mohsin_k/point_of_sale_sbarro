# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Sbarro POS Customizations',
    'version': '1.0.1',
    'category': 'Point Of Sale',
    'sequence': 20,
    'summary': 'Touchscreen Interface for Shops',
    'description': """
Main Features
-------------
* Password Protection on Session Opening and Closing
    """,
    'depends': ['point_of_sale'],
    'data': [
        'wizard/session_closing_view.xml',
        'point_of_sale_view.xml',
        'views/template.xml'
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'application': True,
    'qweb': ['static/src/xml/pos.xml'],
    'website': 'https://www.odoo.com/page/point-of-sale',
    'auto_install': False,
}
