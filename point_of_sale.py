#! /usr/bin/python

# To change this template, choose Tools | Templates
# and open the template in the editor.


from openerp.osv import fields, osv

class pos_config(osv.osv):
    _inherit = 'pos.config'

    _columns = {
        'supervisor_pwd' : fields.char('Supervisor Password'),
        'multi_currency' : fields.boolean('Enable Multicurrency in POS'),
        'currency_ids' : fields.many2many('res.currency','pos_config_currency_id','config_id','currency_id',string='Currencies')
    }