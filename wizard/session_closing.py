# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from openerp.osv import osv, fields
from openerp.exceptions import UserError
from openerp.tools.translate import _

class pos_session_closing(osv.osv_memory):
    _name = 'pos.session.closing'
    _description = 'Session Closing'

    _columns = {
        'password' : fields.char('Password', required=True)
    }

    def accept_password(self, cr, uid, ids, context={}):
        if context is None:context={}
        assert len(ids) == 1, "you can close only one session at time"
        password = self.browse(cr, uid, ids[0]).password
        print 'context',context
        active_id = context.get('active_id', False)
        active_model = context.get('active_model', False)
        if active_id and active_model:
            record = self.pool.get(active_model).browse(cr, uid, active_id)
            supervisor_pwd = record.config_id.supervisor_pwd

            if password != supervisor_pwd:
                raise UserError(_("Invalid Password"))
            else:
                record.signal_workflow('close')
        return {'type' : 'ir.actions.act_window_close' }